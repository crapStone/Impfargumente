# Impfargumente

## Fakten

| | | Quelle |
--- | ---: | :---:
Infektionen (inkl. Dunkelziffer) | 10-15% | [\[1\]][1]
Sterblichkeit | 1% | [\[1\]][1]
Herdenimmunität | >> 70% | [\[1\]][1]
vollständig geimpft (DE) | 67,5% | [\[2\]][2]
vollständig geimpft (Bayern) | 65,6% | [\[2\]][2]
vollständig geimpft (Bremen) | 79,2 % | [\[2\]][2]
Infektion mit Intensivbehandlung (60 Jahre) | 2-3% | [\[1\]][1]
Infektion mit Intensivbehandlung (40 Jahre) | 0,2-0,3% | [\[1\]][1]
Impfschutz | 90-95% | [\[1\]][1]
Impfschutz (nach 5 Monaten) | 50-70% | [\[1\]][1]
Infektion mit Intensivbehandlung trotz Impfung (>60 Jahre)* | 0,2-0,3% | [\[3\]][3]
Infektion mit Intensivbehandlung trotz Impfung (18-59 Jahre)* | 0,01-0,02% | [\[3\]][3]

(* naive Rechnung, könnte nicht ganz stimmen, aber die Tendenz wird klar)

## Pro

- deutlich verringertes Ansteckungsrisiko (bis zu 95%) [\[1\]][1]
- deutlich verringertes Weitergaberisiko (bis zu 85%) [\[1\]][1]
- deutlich verringertes Sterberisiko (keine konkreten Zahlen gefunden)
- deutlich verringertes Risiko eines schweren Verlaufs (bis zu 95%) [\[1\]][1]
- geringere Belegung der wenigen Intensivbetten in Deutschland
- Entlastung der Pflegekräfte
- Die Gesellschaft verlangt es

## Contra

- selten (1-10%) sehr leichte Nebenwirkungen (Rötung, Ausschlag, ...) [\[4\]][4]
- selten (<1%) leichte Nebenwirkungen (Juckreiz, Schwindel, ...) [\[4\]][4]
- selten (<0,1%) schwerere Nebenwirkungen (Schwellungen im Gesicht, akute Gesichtslähmung, ...) [\[4\]][4]
- sehr selten (keine genauen Zahlen bekannt, deutlich unter 0,01%) schwere Nebenwirkungen (anaphylaktische Reaktionen, Herzerkrankungen, ...) [\[4\]][4]

[1]: http://dx.doi.org/10.14279/depositonce-12635
[2]: https://impfdashboard.de/
[3]: https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Situationsberichte/Wochenbericht/Wochenbericht_2021-11-04.pdf?__blob=publicationFile
[4]: https://www.zusammengegencorona.de/impfen/basiswissen-zum-impfen/risiken-und-nebenwirkungen/
